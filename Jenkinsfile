pipeline {
    agent any
    tools {
        nodejs "node"
    }

    environment {
        IMAGE_NAME = "tu-teamprojekt"
    }

    stages {
        stage("Add NPM Dependencies") {
            options {
                retry(3)
                timeout(activity: true, time: 30, unit: 'SECONDS')
            }
            steps {
                sh "npm install"
            }
        }
    
        stage("Compress Sourcecode") {
            parallel {
                stage("Compress CSS Sourcecode") {
                    options {
                        retry(3)
                        timeout(activity: true, time: 10, unit: 'SECONDS')
                    }
                    steps {
                        echo "Compressing css files"

                        script {
                            def cssFiles = findFiles excludes: '', glob: 'public/**/*.css'
                            for (int i = 0; i < cssFiles.size(); i++) {
                                sh "npx uglifycss \"${cssFiles[i].path}\" --output \"${cssFiles[i].path}\""
                            }
                        }
                    }
                }

                stage("Compress Javascript modules") {
                    options {
                        retry(3)
                        timeout(activity: true, time: 10, unit: 'SECONDS')
                    }
                    steps {
                        echo "Compressing mjs files"

                        script {
                            def mjsFiles = findFiles excludes: '**/*.min.mjs', glob: 'public/**/*.mjs'
                            for (int i = 0; i < mjsFiles.size(); i++) {
                                sh "npx uglifyjs \"${mjsFiles[i].path}\" -o \"${mjsFiles[i].path}\" -c -m --module --name-cache mangleNameMjsCache.tmp"
                            }
                            
                            sh "rm -f mangleNameMjsCache.tmp"
                        }
                    }
                }

                stage("Compress Javascript regular") {
                    options {
                        retry(3)
                        timeout(activity: true, time: 10, unit: 'SECONDS')
                    }
                    steps {
                        echo "Compressing js files"

                        script {
                            def jsFiles = findFiles excludes: '**/*.min.js', glob: 'public/**/*.js'
                            for (int i = 0; i < jsFiles.size(); i++) {
                                sh "npx uglifyjs \"${jsFiles[i].path}\" -o \"${jsFiles[i].path}\" -c -m --name-cache mangleNameJsCache.tmp"
                            }
                            
                            sh "rm -f mangleNameJsCache.tmp"
                        }
                    }
                }
            }
        }
        
        stage("Build Image") {
            options {
                lock resource: "TU Teamprojekt"
            }
            steps {
                script {
                    if (env.BRANCH_IS_PRIMARY.equals("true")) {
                        env.X_TAG_BRANCH = ""
                        env.X_TAG_VERSION = "latest"
                    } else if (env.TAG_NAME == null) {
                        env.X_TAG_BRANCH = env.BRANCH_NAME + "-"
                        env.X_TAG_VERSION = "latest"
                    } else {
                        env.X_TAG_BRANCH = "";
                        env.X_TAG_VERSION = env.TAG_NAME
                    }

                    env.X_TAG = env.X_TAG_BRANCH + env.X_TAG_VERSION

                    docker.withRegistry("https://" + env.DOCKER_REGISTRY_HOSTNAME, env.DOCKER_REGISTRY_CREDENTIALS_ID) {
                        def latestImage = docker.build env.IMAGE_NAME
                        latestImage.push(X_TAG)
                    }
                }

                sh "docker image rm $DOCKER_REGISTRY_HOSTNAME/$IMAGE_NAME:$X_TAG"
                sh "docker image rm $IMAGE_NAME"
            }
        }
    }
}
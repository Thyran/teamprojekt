/**
 * @enum {number}
 * @readonly
 */
export const SequenceType = {
    TRANSITION: 0,
    RIDDLE: 1,
    RESULT: 2,
    DONE: 3
};

export default class Sequence {
    /** @type {SequenceType} */ #type = null;
    /** @type {number} */ #videoPauseTime = null;
    /** @type {string} */ #name = null;

    constructor(type, videoPauseTime, name) {
        this.#type = type;
        this.#videoPauseTime = videoPauseTime;
        this.#name = name;
    }

    get data() {
        return {
            type: this.#type,
            pause: this.#videoPauseTime,
            name: this.#name
        };
    }
}
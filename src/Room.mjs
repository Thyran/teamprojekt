import Riddle from "./Riddle.mjs";
import Sequence from "./Sequence.mjs";

export default class Room {
    /** @type {string} */ #video = null;
    /** @type {string} */ #videoType = null;
    /** @type {number} */ #timeLimit = null;
    /** @type {Array<Sequence>} */ #sequences = null;

    constructor(video, videoType, timeLimit) {
        this.#video = video;
        this.#videoType = videoType;
        this.#timeLimit = timeLimit;

        this.#sequences = new Array();
    }

    addSequence(sequence) {
        this.#sequences.push(sequence);
    }

    hasSequence(index) {
        return index >= 0 && index < this.#sequences.length;
    }

    getSequenceData(index) {
        return this.#sequences.at(index).data;
    }

    validateSequence(index, input) {
        const sequence = this.#sequences.at(index);

        if (sequence instanceof Riddle)
            return sequence.validate(input);
        else
            return null;
    }

    get data() {
        return {
            video: this.#video,
            videoType: this.#videoType,
            timeLimit: this.#timeLimit,
            sequences: this.#sequences.map(sequence => sequence.data)
        };
    }

    get sequenceCount() {
        return this.#sequences.length;
    }
}
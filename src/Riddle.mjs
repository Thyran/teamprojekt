/**
 * @typedef RiddleResult 
 * @property {number} stars 
 * @property {number} points 
 * @property {string} message 
 * @property {number} id 
 * 
 * @callback ValidationFunction 
 * @param {string} input 
 * @returns {RiddleResult}
 */

import Sequence, { SequenceType } from "./Sequence.mjs";

/**
 * @type {RiddleResult}
 */
export const DEFAULT_RIDDLE_RETURN = {
    stars: 0,
    points: 0,
    message: "null"
};

export default class Riddle extends Sequence {
    /** @type {number} */ #id = null;
    /** @type {string} */ #query = null;
    /** @type {ValidationFunction} */ #validator = null;

    constructor(id, pauseTime, name, query, validator) {
        super(SequenceType.RIDDLE, pauseTime, name);

        this.#id = id;
        this.#query = query;
        this.#validator = validator;
    }

    validate(input) {
        return Object.assign(this.#validator(input), {
            id: this.#id
        });
    }

    get data() {
        return Object.assign(super.data, {
            id: this.#id,
            query: this.#query
        });
    }
}
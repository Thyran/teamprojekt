import path from "path";
import express from "express";
import root from "../root.js";
import fs from "fs";
import formData from "express-form-data";

import Riddle from "./Riddle.mjs";
import Room from "./Room.mjs";
import Sequence, { SequenceType } from "./Sequence.mjs";

const timecodeToSeconds = function(hours, minutes, seconds, millis) {
    return hours * 60 * 60 + minutes * 60 + seconds + millis / 1000;
};

const room = new Room("videos/Teamprojekt Echo Source.mp4", "video/mp4", 15 * 60);
room.addSequence(new Sequence(SequenceType.TRANSITION, timecodeToSeconds(0, 0, 45, 500), "Begrüßung"));
room.addSequence(new Riddle(1, timecodeToSeconds(0, 1, 31, 50), "Frage 1", "Wie lässt sich die grundlegende Idee des maschinellem Lernens beschreiben? (A|B|C|D)", input => {
    const answer = input.toLowerCase().trim();

    let stars = 0;
    let points = 0;
    let message = "";

    switch (answer) {
        case "a":
            stars = 1;
            points = 65;
            message = "Das wäre wohl eher ein Roboter"
            break;
        case "b":
            stars = 3;
            points = 150;
            message = "Exakt!";
            break;
        case "c":
            stars = 1;
            points = 98;
            message = "Fast, maschinelles Lernen bezieht immer Datengrundlagen ein"
            break;
        case "d":
            stars = 0;
            points = 22;
            message = "Heutzutage Standard für Computer, dies ist leider nicht alles für maschinelles Lernen"
            break;
        default:
            stars = 0;
            points = 0;
            message = "Eine Eingabe zu machen wäre schon von Vorteil!";
            break;
    }

    return {
        stars: stars,
        points: points,
        message: message
    };
}));
room.addSequence(new Sequence(SequenceType.RESULT, timecodeToSeconds(0, 1, 41, 0), null));
room.addSequence(new Sequence(SequenceType.TRANSITION, timecodeToSeconds(0, 3, 8, 250), "Chat-GPT im Studium"));
room.addSequence(new Riddle(2, timecodeToSeconds(0, 3, 24, 500), "Frage 2", "Was sind die potenziellen Vor- und Nachteile der Verwendung von Chat-GPT als persönlicher Tutor im Studium?", input => {
    if (!input) {
        return {
            stars: 0,
            points: 0,
            message: "Nichts zu sagen ist keine Option!"
        };
    }

    return {
        stars: 3,
        points: input.split(" ").length * 10 + 15,
        message: "Interessant!"
    };
}));
room.addSequence(new Sequence(SequenceType.RESULT, timecodeToSeconds(0, 3, 33, 781), null));
room.addSequence(new Riddle(3, timecodeToSeconds(0, 3, 58, 800), "Frage 3", "Stellt dir vor, du bist Entwickler:in von Chat-GPT und möchtest eine einzigartige Funktion hinzufügen, um das Lernen im Studium zu verbessern. Was wäre diese Funktion und warum?", input => {
    if (!input) {
        return {
            stars: 0,
            points: 0,
            message: "Hast du wirklich keine Ideen?"
        };
    }

    return {
        stars: 3,
        points: Math.min(1000, input.length),
        message: "Überaus überzeugend!"
    };
}));
room.addSequence(new Sequence(SequenceType.RESULT, timecodeToSeconds(0, 4, 12, 687), null));
room.addSequence(new Sequence(SequenceType.TRANSITION, timecodeToSeconds(0, 4, 42, 0), "Abschluss"));
room.addSequence(new Sequence(SequenceType.DONE, null, "Passwort"));

const app = express();
app.use(express.static(path.join(root, "public")));

app.use(express.text());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(formData.parse());
app.use(formData.format());

app.get("/videos/:video", (req, res) => {
    const filePath = path.join(root, "videos", req.params.video);

    const stat = fs.statSync(filePath);
    const fileSize = stat.size;

    const range = req.headers.range;

    if (range) {
        const parts = range.replace(/bytes=/, "").split("-");
        const start = parseInt(parts[0], 10);
        const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
        const chunkSize = (end - start) + 1;
        const stream = fs.createReadStream(filePath, { start: start, end: end });

        res.statusCode = 206;
        res.setHeader("Content-Type", "video/mp4");
        res.setHeader("Content-Range", `bytes ${start}-${end}/${fileSize}`);
        res.setHeader("Accept-Ranges", "bytes");
        res.setHeader("Content-Length", chunkSize);

        stream.pipe(res);
    } else
        res.json("Provide range");
});

app.get("/room", (req, res) => {
    res.send(room.data);
});

app.get("/sequence/:sequenceId", (req, res) => {
    const sequenceId = Number.parseInt(req.params.sequenceId);

    res.setHeader("Cache-Control", "no-cache");

    if (!room.hasSequence(sequenceId)) {
        if (sequenceId >= room.sequenceCount) {
            res.statusMessage = "All sequences done";
            res.sendStatus(201);
        } else {
            res.statusMessage = "Sequence not found";
            res.sendStatus(404);
        }

        return;
    }

    const data = room.getSequenceData(sequenceId);
    
    res.contentType("application/json");
    res.send({
        sequenceId: sequenceId,
        data: data
    });
});

app.post("/validate", (req, res) => {
    const sequenceId = Number.parseInt(req.body.sequenceId);
    const input = req.body.input;

    res.contentType("application/json");

    if (typeof input === "undefined" || input == null) {
        res.statusMessage = "Not a valid request";
        res.sendStatus(400);
    } else if (!room.hasSequence(sequenceId)) {
        res.statusMessage = "Sequence not found";
        res.sendStatus(404);
    } else {
        const result = room.validateSequence(sequenceId, input);

        if (!result) {
            res.sendStatus(501);
            return;
        }

        res.send(Object.assign(result, {
            nextSequenceId: sequenceId + 1
        }));
    }
});

const port = process.env.PORT || 80;
app.listen(port, () => {
    console.log("Server listens on port " + port);
});
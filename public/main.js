const form = document.getElementById("prompt");
const query = document.getElementById("query");
const enterForm = document.getElementById("enter-form");
const finalForm = document.getElementById("final-form");

const stars = document.getElementById("stars");
const points = document.getElementById("points");
const message = document.getElementById("message");
const cont = document.getElementById("continue");

const main = document.getElementById("main");
const enter = document.getElementById("enter");
const resultScreen = document.getElementById("result");
const endScreen = document.getElementById("end");
const finalScreen = document.getElementById("final");

const replay = document.getElementById("replay");
const endPoints = document.getElementById("end-points");

const totalScore = document.getElementById("total-score");
const questionId = document.getElementById("question-id");

const star0 = stars.querySelector(".star[star-id='0']");
const star1 = stars.querySelector(".star[star-id='1']");
const star2 = stars.querySelector(".star[star-id='2']");

const totalStarsDisplay = document.getElementById("total-stars");

const mainContent = document.getElementById("main-content");
const content = document.getElementById("content");

const chapters = document.getElementById("chapters");

let score = 0;
let riddleScoreMap = new Map();
let riddleStarMap = new Map();
let totalStars = 0;
let totalPossibleStars = 0;
let allowInputSubmission = false;

function showRiddle(data) {
    lastRiddleSequenceIndex = currentSequenceIndex;

    allowInputSubmission = false;

    star0.classList.add("failed");
    star1.classList.add("failed");
    star2.classList.add("failed");

    form.reset();
    cont.reset();

    questionId.innerHTML = data.id;
    query.innerHTML = data.query;
}

function updateTotalStars() {
    for (const starScore of totalStarsDisplay.querySelectorAll(".star-score")) {
        const riddleId = starScore.getAttribute("riddle-id");
        const stars = riddleStarMap.get(Number.parseInt(riddleId));

        for (const star of starScore.querySelectorAll(".star-score-element"))
            star.setAttribute("reached", star.getAttribute("star-index") < stars);
    }

    totalStars = 0;

    for (const starCount of riddleStarMap.values())
        totalStars += starCount;
}

function updateTotalScore() {
    score = 0;

    for (const points of riddleScoreMap.values())
        score += points;
    
    totalScore.innerHTML = String(score).padStart(4, "0");
}

function receiveValidationResult(event) {
    const response = event.target;
    const result = response.response;

    if (response.status !== 200)
        return;

    const json = JSON.parse(result);

    star0.classList.toggle("failed", !(json.stars > 0));
    star1.classList.toggle("failed", !(json.stars > 1));
    star2.classList.toggle("failed", !(json.stars > 2));

    points.innerHTML = String(json.points).padStart(4, "0");
    message.innerHTML = json.message;

    if (!riddleScoreMap.has(json.id) || riddleScoreMap.has(json.id) && riddleScoreMap.get(json.id) < json.points)
        riddleScoreMap.set(json.id, json.points);

    
    if (!riddleStarMap.has(json.id) || riddleStarMap.has(json.id) && riddleStarMap.get(json.id) < json.stars)
        riddleStarMap.set(json.id, json.stars);

    allowInputSubmission = false;
    autoplayNextSequence = false;
    
    loadSequence(json.nextSequenceId);
}

let input = null;

form.addEventListener("submit", function(event) {
    event.preventDefault();
    event.stopPropagation();

    const mode = event.target.submitted;

    if (mode === "repeat") {
        loadLastRiddleSequence();
        return;
    }

    if (!allowInputSubmission) {
        showNotification(NotificationType.INFO, "Bitte höre Echo zu Ende zu", 1500);
        return;
    }

    const data = new FormData(form);
    data.append("sequenceId", currentSequenceIndex);

    const request = new XMLHttpRequest();
    request.open("POST", "/validate");
    request.onload = receiveValidationResult;
    request.onerror = console.error;
    request.send(data);
});

cont.addEventListener("submit", function(event) {
    event.preventDefault();
    event.stopPropagation();

    const mode = event.target.submitted;

    if (mode === "retry") {
        loadLastRiddleSequence();
        return;
    }

    if (!allowInputSubmission) {
        showNotification(NotificationType.INFO, "Bitte höre Echo zu Ende zu", 1500);
        return;
    }
    
    updateTotalStars();
    updateTotalScore();

    autoplayNextSequence = true;
    loadNextSequence();
});

replay.addEventListener("submit", function(event) {
    event.preventDefault();
    event.stopPropagation();

    location.reload();
});

resultScreen.addEventListener("transitionend", function(event) {
    if (event.propertyName !== "top" || !event.target.classList.contains("open"))
        return;
    
    cont.classList.remove("hidden");
});

const echo = document.getElementById("echo");
const source = echo.querySelector("source");

const endMessage = document.getElementById("end-message");

function showEndScreen()  {
    endMessage.innerHTML = endReason;

    main.classList.add("hidden");
    resultScreen.classList.remove("open");
    endScreen.classList.add("open");
    finalScreen.classList.remove("open");
}

const starsCollectedDisplay = document.getElementById("stars-collected");
const starsCollectableDisplay = document.getElementById("stars-collectable");
const starsPercentDisplay = document.getElementById("stars-percent");

const passwordHintDisplay = document.getElementById("solution-hint");

function generateSolutionHint(totalStars, totalPossibleStars) {
    const freeStars = Math.floor(totalPossibleStars * 0.1) + 1; // CHECK scale this rate better with more or less stars
    const neededStars = totalPossibleStars - freeStars;

    const starRate = neededStars <= 0 ? 1 : Math.max(0, Math.min(totalStars / neededStars, 1));
    const hintCount = Math.floor(starRate * roomPassword.length);

    const hintIndices = new Array();
    for (let i = 0; i < hintCount; i++) {
        let newIndex = null;

        while (newIndex === null || hintIndices.includes(newIndex))
            newIndex = Math.floor(Math.random() * (roomPassword.length));
        
        hintIndices.push(newIndex);
    }

    let hint = "";

    for (let i = 0; i < roomPassword.length; i++)
        hint += hintIndices.includes(i) ? roomPassword[i] : "_";

    return hint;
}

function showFinalScreen() {
    main.classList.add("hidden");
    resultScreen.classList.remove("open");
    endScreen.classList.remove("open");
    finalScreen.classList.add("open");

    starsCollectedDisplay.innerHTML = String(totalStars).padStart(2, "0");
    starsCollectableDisplay.innerHTML = String(totalPossibleStars).padStart(2, "0");
    starsPercentDisplay.innerHTML = String((totalStars / totalPossibleStars * 100).toFixed(2)).padStart(6, "0") + "%";

    passwordHintDisplay.innerHTML = generateSolutionHint(totalStars, totalPossibleStars);
}

function showScreen(sequenceType) {
    echo.classList.remove("hidden");
    echo.setAttribute("ended", false);

    endScreen.classList.remove("open");
    finalScreen.classList.remove("open");
    
    switch (sequenceType) {
        case 0:
            main.classList.add("hidden");
            resultScreen.classList.remove("open");
            break;
        case 1:
            main.classList.remove("hidden");
            resultScreen.classList.remove("open");
            break;
        case 2:
            main.classList.add("hidden");
            resultScreen.classList.add("open");
            break;
        default:
            throw new Error("Sequence type not known: " + sequenceType);
    }
}

const loadNextSequence = () => {
    let lastChapterSequenceIndex;

    if (currentSequence.type === 2)
        lastChapterSequenceIndex = lastRiddleSequenceIndex;
    else if (currentSequence.type !== 1)
        lastChapterSequenceIndex = currentSequenceIndex;
    
    if (currentSequence.type !== 1) {
        const chapter = chapters.querySelector("[sequence-id='" + lastChapterSequenceIndex + "']");
        chapter.setAttribute("done", true);
    }
    
    loadSequence(currentSequenceIndex + 1, false);
};

const loadLastRiddleSequence = () => { loadSequence(lastRiddleSequenceIndex); };

let roomData = null;
let roomPassword = null;

let currentSequence = null;
let currentSequenceIndex = 0;
let autoplayNextSequence = true;
let lastRiddleSequenceIndex = -1;
let endReason = null;

function sequenceLoadable(index) {
    return roomData.sequences[index].type !== 2;
}

function loadSequence(index, forceLoad = true) {
    echo.pause();

    for (const chapter of chapters.querySelectorAll("[sequence-id"))
        chapter.classList.toggle("current", chapter.getAttribute("sequence-id") == index);

    const nextSequence = roomData.sequences[index];

    if (index >= roomData.sequences.length || nextSequence.type === 3) {
        content.setAttribute("mode", "3"); // set to end mode
        showFinalScreen();
        return;
    }

    if (forceLoad) {
        const prevTime = index === 0 ? 0 : roomData.sequences[index - 1].pause;
        echo.currentTime = prevTime;
    }

    currentSequence = roomData.sequences[index];
    currentSequenceIndex = index;

    content.setAttribute("mode", currentSequence.type);

    autoplayNextSequence = currentSequence.type === 0;

    if (currentSequence.type === 1)
        showRiddle(currentSequence);

    echo.setAttribute("sequence-id", index);

    showScreen(currentSequence.type);

    echo.play();
}

function startEcho() {
    if (echo.readyState === 4) {
        allowInputSubmission = false;

        echo.setAttribute("sequence-id", 0);
        echo.setAttribute("ended", false);

        echo.classList.remove("hidden");
    } else
        setTimeout(startEcho, 100);
}

function endEcho(reason) {
    endReason = reason;

    echo.pause();
    echo.setAttribute("ended", true);

    echo.classList.add("hidden");
}

echo.ontimeupdate = function(event) {
    const target = event.target;

    if (!currentSequence)
        return;

    if (target.currentTime >= currentSequence.pause) {
        if (autoplayNextSequence)
            loadNextSequence();
        else {
            target.pause();

            if (currentSequence.type === 1)
                allowInputSubmission = true;
            else if (currentSequence.type === 2)
                allowInputSubmission = true;
        }
    }
}

totalScore.innerHTML = String(score).padStart(4, "0");

function displayChapters(sequences) {
    let chapterId = 0;

    sequences.forEach((sequence, index) => {
        if (!sequenceLoadable(index))
            return;

        const chapter = document.createElement("li");
        chapter.innerHTML = sequence.name;

        chapter.setAttribute("chapter-id", ++chapterId);
        chapter.setAttribute("chapter-type", sequence.type);
        chapter.setAttribute("sequence-id", index);

        chapter.addEventListener("click", function(event) {
            event.preventDefault();
            event.stopPropagation();

            inforbar.classList.remove("open");

            loadSequence(Number.parseInt(event.target.getAttribute("sequence-id")));
        });

        chapters.appendChild(chapter);
    });
}

const riddleStarCount = 3;

function displayTotalStars(riddles) {
    totalPossibleStars = riddles.length * riddleStarCount;

    riddles.forEach(riddle => {
        const starDisplay = document.createElement("div");
        starDisplay.classList.add("star-score");
        starDisplay.setAttribute("riddle-id", riddle.id);

        for (let i = 0; i < riddleStarCount; i++) {
            const star = document.createElement("span");
            star.classList.add("star-score-element");
            star.setAttribute("star-index", i);
            star.setAttribute("reached", false);

            starDisplay.appendChild(star);
        }

        totalStarsDisplay.appendChild(starDisplay);
    });
}

let timeIntervalId = null;
let timeRemaining = null;

const passwordMaxLength = 10;
const passwordMinLength = 4;

function generateRoomPassword() {
    const length = Math.floor(Math.random() * (passwordMaxLength - passwordMinLength + 1)) + passwordMinLength;

    const asciiStart = 65;
    const asciiEnd = 90;

    let password = "";

    for (let i = 0; i < length; i++) {
        const randomLetter = Math.floor(Math.random() * (asciiEnd - asciiStart + 1)) + asciiStart;
        const letter = String.fromCharCode(randomLetter);

        password += letter;
    }

    return password;
}

enterForm.addEventListener("submit", function(event) {
    event.preventDefault();
    event.stopPropagation();

    const loadRoomRequest = new XMLHttpRequest();
    loadRoomRequest.open("GET", "/room");
    loadRoomRequest.onload = function() {
        roomData = JSON.parse(loadRoomRequest.response);
        timeRemaining = roomData.timeLimit;

        if (roomData.sequences.length < 1)
            throw new Error("No sequences in current room");
        
        displayChapters(roomData.sequences);
        displayTotalStars(roomData.sequences.filter(sequence => sequence.type === 1));

        echo.setAttribute("src", roomData.video);
        echo.setAttribute("type", roomData.videoType);

        echo.load();
        startEcho();

        roomPassword = generateRoomPassword();
        timeDisplay.innerHTML = secondsToString(timeRemaining);

        enter.classList.add("hidden");
    };
    loadRoomRequest.send();
});

const timeDisplay = document.getElementById("time");

function secondsToString(seconds) {
    const timeHours = Math.floor(seconds / (60 ** 2)) % 24;
    const timeMinutes = Math.floor(seconds / 60) % 60;
    const timeSeconds = Math.floor(seconds) % 60;

    const timeHourString = String(timeHours).padStart(2, "0");
    const timeMinuteString = String(timeMinutes).padStart(2, "0");
    const timeSecondString = String(timeSeconds).padStart(2, "0");

    return timeHourString + ":" + timeMinuteString + ":" + timeSecondString;
}

function countdown() {
    timeDisplay.innerHTML = secondsToString(--timeRemaining);

    if (timeRemaining < 60)
        timeDisplay.setAttribute("critical", true);
    
    if (timeRemaining <= 0) {
        clearInterval(timeIntervalId);
        endEcho("Leider ist die Zeit abgelaufen. Aber mach dir nichts draus, du kannst es jederzeit erneut versuchen!");
    }
}

function startEscapeRoom() {
    timeIntervalId = setInterval(countdown, 1000);
    loadSequence(Number.parseInt(echo.getAttribute("sequence-id")));
}

echo.addEventListener("transitionend", function(event) {
    if (event.propertyName === "transform" && echo.hasAttribute("ended")) {
        if (echo.getAttribute("ended") !== "true")
            startEscapeRoom();
        else
            showEndScreen();
    }
});

const inforbar = document.getElementById("infobar");
const infobarCollapse = document.getElementById("infobar-collapse");
infobarCollapse.addEventListener("click", function(event) {
    event.preventDefault();
    event.stopPropagation();

    inforbar.classList.toggle("open");
});

document.addEventListener("click", function(event) {
    inforbar.classList.remove("open");
});

finalForm.addEventListener("submit", function(event) {
    event.preventDefault();
    event.stopPropagation();

    const input = event.target.querySelector("input[type='text']").value.toLowerCase();
    const check = roomPassword.toLowerCase();

    if (input === check) {
        clearInterval(timeIntervalId);

        score += Math.floor(timeRemaining / 5);

        totalScore.innerHTML = String(score).padStart(4, "0");
        endPoints.innerHTML = score;
        finalScreen.classList.remove("open");

        setTimeout(() => {
            endEcho("Herzlichen Glückwunsch, du hast die Lektion erfolgreich abgeschlossen!");
        }, 100);
    } else
        showNotification(NotificationType.ERROR, "Das Passwort ist nicht korrekt, verdiene mehr Sterne um bessere Lösungshinweise zu erhalten");

    event.target.reset();
});

/**
 * @enum {string}
 */
const NotificationType = {
    SUCCESS: 0,
    ERROR: 1,
    WARNING: 2,
    INFO: 3
};

function getNotificationTitle(type) {
    switch (type) {
        case NotificationType.SUCCESS: return "Erfolg";
        case NotificationType.ERROR: return "Fehler";
        case NotificationType.WARNING: return "Warnung";
        case NotificationType.INFO: return "Info";
    }
}

let notificationTimeout = null;

/**
 * 
 * @param {NotificationType} type 
 * @param {string} message 
 */
function showNotification(type, message, uptime = 5000) {
    const notification = document.getElementById("notification");

    notification.querySelector(".notification-title").innerHTML = getNotificationTitle(type);
    notification.querySelector(".notification-message").innerHTML = message;
    notification.setAttribute("type", type);
    notification.classList.add("open");

    clearTimeout(notificationTimeout);
    notificationTimeout = setTimeout(() => {
        notification.classList.remove("open");
    }, uptime);
}
FROM node:20-slim

ADD . /opt/app
WORKDIR /opt/app

RUN npm install

EXPOSE 80
ENTRYPOINT ["node", "."]